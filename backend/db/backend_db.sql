PGDMP          *            	    {         
   backend_db    15.4    15.4 %               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16469 
   backend_db    DATABASE        CREATE DATABASE backend_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Bolivia.1252';
    DROP DATABASE backend_db;
                postgres    false                       0    0    DATABASE backend_db    ACL     -   GRANT ALL ON DATABASE backend_db TO db_user;
                   postgres    false    3355            �            1259    16499    persona_juridica    TABLE     �   CREATE TABLE public.persona_juridica (
    id integer NOT NULL,
    id_usuario_financiero integer,
    razon_social character(50),
    nit integer,
    "dirección" character(200),
    telefono integer
);
 $   DROP TABLE public.persona_juridica;
       public         heap    postgres    false            �            1259    16498    persona_juridica_id_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_juridica_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.persona_juridica_id_seq;
       public          postgres    false    219                       0    0    persona_juridica_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.persona_juridica_id_seq OWNED BY public.persona_juridica.id;
          public          postgres    false    218            �            1259    16489    persona_natural    TABLE     3  CREATE TABLE public.persona_natural (
    id integer NOT NULL,
    id_usuario_financiero integer,
    nombres character(60) NOT NULL,
    primer_apellido character(100),
    segundo_apellido character(100),
    nro_documento_identidad integer,
    celular integer,
    direccion_domicilio character(200)
);
 #   DROP TABLE public.persona_natural;
       public         heap    postgres    false            �            1259    16488    persona_natural_id_seq    SEQUENCE     �   CREATE SEQUENCE public.persona_natural_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.persona_natural_id_seq;
       public          postgres    false    217                       0    0    persona_natural_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.persona_natural_id_seq OWNED BY public.persona_natural.id;
          public          postgres    false    216            �            1259    16472    usuario_financiero    TABLE     �   CREATE TABLE public.usuario_financiero (
    id integer NOT NULL,
    fecha_creacion date NOT NULL,
    usuario_creacion integer,
    fecha_modificacion date,
    usuario_modificacion integer
);
 &   DROP TABLE public.usuario_financiero;
       public         heap    postgres    false                       0    0    TABLE usuario_financiero    COMMENT     Y   COMMENT ON TABLE public.usuario_financiero IS 'creacion de la tabla usuario_financiero';
          public          postgres    false    215            �            1259    16471    usuario_financiero_id_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_financiero_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.usuario_financiero_id_seq;
       public          postgres    false    215                        0    0    usuario_financiero_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.usuario_financiero_id_seq OWNED BY public.usuario_financiero.id;
          public          postgres    false    214            �            1259    16528    usuario_general    TABLE     �   CREATE TABLE public.usuario_general (
    id integer NOT NULL,
    usuario character(10) NOT NULL,
    clave character(10) NOT NULL,
    rol integer NOT NULL
);
 #   DROP TABLE public.usuario_general;
       public         heap    postgres    false            !           0    0    TABLE usuario_general    COMMENT     �   COMMENT ON TABLE public.usuario_general IS 'usuario general, seria el que que controla o las personas que controlan el sistema';
          public          postgres    false    221            �            1259    16527    usuario_general_id_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_general_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.usuario_general_id_seq;
       public          postgres    false    221            "           0    0    usuario_general_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.usuario_general_id_seq OWNED BY public.usuario_general.id;
          public          postgres    false    220            v           2604    16502    persona_juridica id    DEFAULT     z   ALTER TABLE ONLY public.persona_juridica ALTER COLUMN id SET DEFAULT nextval('public.persona_juridica_id_seq'::regclass);
 B   ALTER TABLE public.persona_juridica ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    219    219            u           2604    16492    persona_natural id    DEFAULT     x   ALTER TABLE ONLY public.persona_natural ALTER COLUMN id SET DEFAULT nextval('public.persona_natural_id_seq'::regclass);
 A   ALTER TABLE public.persona_natural ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216    217            t           2604    16475    usuario_financiero id    DEFAULT     ~   ALTER TABLE ONLY public.usuario_financiero ALTER COLUMN id SET DEFAULT nextval('public.usuario_financiero_id_seq'::regclass);
 D   ALTER TABLE public.usuario_financiero ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214    215            w           2604    16531    usuario_general id    DEFAULT     x   ALTER TABLE ONLY public.usuario_general ALTER COLUMN id SET DEFAULT nextval('public.usuario_general_id_seq'::regclass);
 A   ALTER TABLE public.usuario_general ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    221    221                      0    16499    persona_juridica 
   TABLE DATA           p   COPY public.persona_juridica (id, id_usuario_financiero, razon_social, nit, "dirección", telefono) FROM stdin;
    public          postgres    false    219   ;+                 0    16489    persona_natural 
   TABLE DATA           �   COPY public.persona_natural (id, id_usuario_financiero, nombres, primer_apellido, segundo_apellido, nro_documento_identidad, celular, direccion_domicilio) FROM stdin;
    public          postgres    false    217   X+                 0    16472    usuario_financiero 
   TABLE DATA           |   COPY public.usuario_financiero (id, fecha_creacion, usuario_creacion, fecha_modificacion, usuario_modificacion) FROM stdin;
    public          postgres    false    215   u+                 0    16528    usuario_general 
   TABLE DATA           B   COPY public.usuario_general (id, usuario, clave, rol) FROM stdin;
    public          postgres    false    221   �+       #           0    0    persona_juridica_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.persona_juridica_id_seq', 1, false);
          public          postgres    false    218            $           0    0    persona_natural_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.persona_natural_id_seq', 1, false);
          public          postgres    false    216            %           0    0    usuario_financiero_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.usuario_financiero_id_seq', 10, true);
          public          postgres    false    214            &           0    0    usuario_general_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.usuario_general_id_seq', 2, true);
          public          postgres    false    220            {           2606    16504 &   persona_juridica persona_juridica_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.persona_juridica
    ADD CONSTRAINT persona_juridica_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.persona_juridica DROP CONSTRAINT persona_juridica_pkey;
       public            postgres    false    219            y           2606    16477 *   usuario_financiero usuario_financiero_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.usuario_financiero
    ADD CONSTRAINT usuario_financiero_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.usuario_financiero DROP CONSTRAINT usuario_financiero_pkey;
       public            postgres    false    215            }           2606    16533 $   usuario_general usuario_general_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.usuario_general
    ADD CONSTRAINT usuario_general_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.usuario_general DROP CONSTRAINT usuario_general_pkey;
       public            postgres    false    221            ~           2606    16493 #   persona_natural idUsuarioFinanciero    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona_natural
    ADD CONSTRAINT "idUsuarioFinanciero" FOREIGN KEY (id_usuario_financiero) REFERENCES public.usuario_financiero(id);
 O   ALTER TABLE ONLY public.persona_natural DROP CONSTRAINT "idUsuarioFinanciero";
       public          postgres    false    3193    217    215            '           0    0 3   CONSTRAINT "idUsuarioFinanciero" ON persona_natural    COMMENT     �   COMMENT ON CONSTRAINT "idUsuarioFinanciero" ON public.persona_natural IS 'llave secundaria proveniente de la tavla usuario financiero';
          public          postgres    false    3198                       2606    16505 $   persona_juridica idUsuarioFinanciero    FK CONSTRAINT     �   ALTER TABLE ONLY public.persona_juridica
    ADD CONSTRAINT "idUsuarioFinanciero" FOREIGN KEY (id_usuario_financiero) REFERENCES public.usuario_financiero(id);
 P   ALTER TABLE ONLY public.persona_juridica DROP CONSTRAINT "idUsuarioFinanciero";
       public          postgres    false    219    3193    215                  x������ � �            x������ � �         =   x�3�4202�54�54�4��".#l���M�	�b4�&h�M���%6ACl�1z\\\ ��&�         #   x�3�,-N-2T d�!��k���2��b���� {U�     