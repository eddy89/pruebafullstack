package useraback.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Service;
import useraback.test.model.PersonaNatural;
import useraback.test.repository.PersonaNaturalRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
public class PersonaNaturalService implements PersonaNaturalRepository {
    @Autowired
    private PersonaNaturalRepository personaNaturalRepository;

    @Override
    public void flush() {

    }

    @Override
    public <S extends PersonaNatural> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends PersonaNatural> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<PersonaNatural> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Integer> integers) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public PersonaNatural getOne(Integer integer) {
        return null;
    }

    @Override
    public PersonaNatural getById(Integer integer) {
        return null;
    }

    @Override
    public PersonaNatural getReferenceById(Integer integer) {
        return null;
    }

    @Override
    public <S extends PersonaNatural> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends PersonaNatural> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends PersonaNatural> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends PersonaNatural> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends PersonaNatural> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends PersonaNatural> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends PersonaNatural, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }

    @Override
    public <S extends PersonaNatural> S save(S entity) {
        return personaNaturalRepository.save(entity);
    }

    @Override
    public <S extends PersonaNatural> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<PersonaNatural> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return (personaNaturalRepository.existsById(integer))? true: false;
    }

    @Override
    public List<PersonaNatural> findAll() {
        return personaNaturalRepository.findAll();
    }

    public List<PersonaNatural> getAllPersonsApePat(String apellido){
        List<PersonaNatural> personaResAp = new ArrayList<>();
        List<PersonaNatural> personaNaturals = personaNaturalRepository.findAll();
        for (Integer i = 0; i < personaNaturals.size();i++)
        {
            if(personaNaturals.get(i).getPrimerApellido() == apellido){
                personaResAp.add(personaNaturals.get(i));
            }
        }
        return personaResAp;
    }

    public List<PersonaNatural> getAllPersonsApeMat(String apellido){
        List<PersonaNatural> personaResAp = new ArrayList<>();
        List<PersonaNatural> personaNaturals = personaNaturalRepository.findAll();
        for (Integer i = 0; i < personaNaturals.size();i++)
        {
            if(personaNaturals.get(i).getSegundoApellido() == apellido){
                personaResAp.add(personaNaturals.get(i));
            }
        }
        return personaResAp;
    }

    public List<PersonaNatural> getAllPersonsNombres(String nombres){
        List<PersonaNatural> personaResAp = new ArrayList<>();
        List<PersonaNatural> personaNaturals = personaNaturalRepository.findAll();
        for (Integer i = 0; i < personaNaturals.size();i++)
        {
            if(personaNaturals.get(i).getNombres() == nombres){
                personaResAp.add(personaNaturals.get(i));
            }
        }
        return personaResAp;
    }

    public List<PersonaNatural> getPersonNroIdentidad(Integer numero){
        List<PersonaNatural> personaResAp = new ArrayList<>();
        List<PersonaNatural> personaNaturals = personaNaturalRepository.findAll();
        for (int i = 0; i < personaNaturals.size();i++)
        {
            if(personaNaturals.get(i).getNroDocumentoIdentidad() == numero){
                personaResAp.add(personaNaturals.get(i));
            }
        }
        return personaResAp;
    }

    @Override
    public List<PersonaNatural> findAllById(Iterable<Integer> integers) {
        return personaNaturalRepository.findAllById(integers);
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {
        personaNaturalRepository.deleteById(integer);
    }

    @Override
    public void delete(PersonaNatural entity) {
        personaNaturalRepository.delete(entity);
    }

    @Override
    public void deleteAllById(Iterable<? extends Integer> integers) {

    }

    @Override
    public void deleteAll(Iterable<? extends PersonaNatural> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<PersonaNatural> findAll(Sort sort) {
        return personaNaturalRepository.findAll(sort);
    }

    @Override
    public Page<PersonaNatural> findAll(Pageable pageable) {
        return personaNaturalRepository.findAll(pageable);
    }
}
