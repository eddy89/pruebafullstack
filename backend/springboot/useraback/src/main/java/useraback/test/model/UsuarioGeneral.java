package useraback.test.model;

import jakarta.persistence.*;

import java.io.Serializable;


@Entity
@Table(name = "usuario_general")
public class UsuarioGeneral implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String usuario;
    private String clave;

    public UsuarioGeneral() {
    }

    public UsuarioGeneral(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}