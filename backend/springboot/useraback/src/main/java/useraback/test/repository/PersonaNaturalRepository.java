package useraback.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import useraback.test.model.PersonaNatural;

public interface PersonaNaturalRepository extends JpaRepository<PersonaNatural, Integer> {

}