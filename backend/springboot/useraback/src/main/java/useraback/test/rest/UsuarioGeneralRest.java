package useraback.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import useraback.test.model.UsuarioGeneral;
import useraback.test.service.UsuarioGeneralService;

import java.util.List;

@RestController
@RequestMapping("/generales/")
public class UsuarioGeneralRest {

    @Autowired
    private UsuarioGeneralService usuarioGeneralService;

    @GetMapping
    private ResponseEntity<List<UsuarioGeneral>> getAllUsuariosGenerales(){
        return ResponseEntity.ok(usuarioGeneralService.findAll());
    }
}
