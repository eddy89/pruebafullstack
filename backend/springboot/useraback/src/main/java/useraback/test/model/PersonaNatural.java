package useraback.test.model;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table (name = "persona_natural")
public class PersonaNatural implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer idUsuarioFinanciero;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private Integer nroDocumentoIdentidad;
    private Integer celular;
    private String direccionDomicilio;

    public PersonaNatural(){}

    public PersonaNatural(Integer idUsuarioFinanciero, String nombres, String primerApellido, String segundoApellido, Integer nroDocumentoIdentidad, Integer celular, String direccionDomicilio) {
        this.idUsuarioFinanciero = idUsuarioFinanciero;
        this.nombres = nombres;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.nroDocumentoIdentidad = nroDocumentoIdentidad;
        this.celular = celular;
        this.direccionDomicilio = direccionDomicilio;
    }

    public Integer getIdUsuarioFinanciero() {
        return idUsuarioFinanciero;
    }

    public void setIdUsuarioFinanciero(Integer idUsuarioFinanciero) {
        this.idUsuarioFinanciero = idUsuarioFinanciero;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Integer getNroDocumentoIdentidad() {
        return nroDocumentoIdentidad;
    }

    public void setNroDocumentoIdentidad(Integer nroDocumentoIdentidad) {
        this.nroDocumentoIdentidad = nroDocumentoIdentidad;
    }

    public Integer getCelular() {
        return celular;
    }

    public void setCelular(Integer celular) {
        this.celular = celular;
    }

    public String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    public void setDireccionDomicilio(String direccionDomicilio) {
        this.direccionDomicilio = direccionDomicilio;
    }
}
