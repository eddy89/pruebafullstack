package useraback.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Service;
import useraback.test.model.UsuarioGeneral;
import useraback.test.repository.UsuarioGeneralRepository;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
public class UsuarioGeneralService implements UsuarioGeneralRepository{

    @Autowired
    private UsuarioGeneralRepository usuarioGeneralRepository;

    @Override
    public void flush() {

    }

    @Override
    public <S extends UsuarioGeneral> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<UsuarioGeneral> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Integer> integers) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public UsuarioGeneral getOne(Integer integer) {
        return null;
    }

    @Override
    public UsuarioGeneral getById(Integer integer) {
        return null;
    }

    @Override
    public UsuarioGeneral getReferenceById(Integer integer) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends UsuarioGeneral> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends UsuarioGeneral> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends UsuarioGeneral, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> S save(S entity) {
        return null;
    }

    @Override
    public <S extends UsuarioGeneral> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<UsuarioGeneral> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public List<UsuarioGeneral> findAll() {
        return usuarioGeneralRepository.findAll();
    }

    @Override
    public List<UsuarioGeneral> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(UsuarioGeneral entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Integer> integers) {

    }

    @Override
    public void deleteAll(Iterable<? extends UsuarioGeneral> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<UsuarioGeneral> findAll(Sort sort) {
        return usuarioGeneralRepository.findAll(sort);
    }

    @Override
    public Page<UsuarioGeneral> findAll(Pageable pageable) {
        return null;
    }
}
