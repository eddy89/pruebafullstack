package useraback.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import useraback.test.model.UsuarioGeneral;

public interface UsuarioGeneralRepository extends JpaRepository<UsuarioGeneral, Integer> {
}