package useraback.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import useraback.test.model.UsuarioFinanciero;

public interface UsuarioFinancieroRepository extends JpaRepository<UsuarioFinanciero, Integer> {
}