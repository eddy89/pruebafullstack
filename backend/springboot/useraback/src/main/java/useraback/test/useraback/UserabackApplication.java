package useraback.test.useraback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserabackApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserabackApplication.class, args);
	}

}
