package useraback.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import useraback.test.model.PersonaNatural;
import useraback.test.service.PersonaNaturalService;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping ("/naturales/")
public class PersonaNaturalRest {

    @Autowired
    private PersonaNaturalService personaNaturalService;

    @GetMapping
    private ResponseEntity<List<PersonaNatural>> getAllPersonasNaturales(){
        return  ResponseEntity.ok(personaNaturalService.findAll());
    }

    @GetMapping ("{primerApellido}")
    private ResponseEntity<List<PersonaNatural>> getAllPersonsApePat(@PathVariable("primerApellido") String apellido){
        return ResponseEntity.ok(personaNaturalService.getAllPersonsApePat(apellido));
    }

    @GetMapping ("{segundoApellido}")
    private ResponseEntity<List<PersonaNatural>> getAllPersonsApeMat(@PathVariable("segundoApellido") String apellido) {
        return ResponseEntity.ok(personaNaturalService.getAllPersonsApeMat(apellido));
    }

    @GetMapping ("{nombres}")
    private ResponseEntity<List<PersonaNatural>> getAllPersonsNombres(@PathVariable("nombres") String nombres) {
        return ResponseEntity.ok(personaNaturalService.getAllPersonsNombres(nombres));
    }

    @GetMapping ("{numero}")
    private ResponseEntity<List<PersonaNatural>> getPersonsNroIdentidad(@PathVariable("numero") Integer numero) {
        return ResponseEntity.ok(personaNaturalService.getPersonNroIdentidad(numero));
    }

    @PostMapping
    private ResponseEntity<PersonaNatural> savePersonaNatural(@RequestBody PersonaNatural personaNatural) {
        try {
            PersonaNatural savePerson = personaNaturalService.save(personaNatural);
            return ResponseEntity.created(new URI("/naturales/" + savePerson.getNroDocumentoIdentidad())).body(savePerson);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

}
