package useraback.test.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import useraback.test.model.UsuarioFinanciero;
import useraback.test.service.UsuarioFinancieroService;

import java.util.List;

@RestController
@RequestMapping("/financieros/")
public class UsuarioFinancieroRest {

    @Autowired
    private UsuarioFinancieroService usuarioFinancieroService;

    @GetMapping
    private ResponseEntity<List<UsuarioFinanciero>> getAllUsuarioFinanciero(){
        return ResponseEntity.ok(usuarioFinancieroService.findAll());
    }
}
